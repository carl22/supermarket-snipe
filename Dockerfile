# Firefox over VNC
#
# VERSION               0.1
# DOCKER-VERSION        0.2

FROM    node

# Install vnc, xvfb in order to create a 'fake' display and firefox
RUN     apt-get update
RUN apt-get install -y xvfb fluxbox x11vnc dbus libasound2 libqt4-dbus libqt4-network libqtcore4 libqtgui4 libxss1 \
 libpython2.7 libqt4-xml libaudio2 libmng1 fontconfig lib32stdc++6 lib32z1 lib32ncurses5 libc6-i386 lib32gcc1 nano \
 libnss3-dev libatk-bridge2.0 python-gobject-2 curl git libgtk-3-0 xfce4-terminal

RUN echo 'kernel.unprivileged_userns_clone=1' > /etc/sysctl.d/userns.conf

RUN useradd -d /app -u 9876 snipe
# Change to non-root privilege
RUN mkdir /app && chown snipe:snipe /app
WORKDIR /app

USER snipe

COPY . /app
RUN npm i
# Make sure the package repository is up to date
RUN     mkdir ~/.vnc
# Setup a password
RUN     x11vnc -storepasswd 1234 ~/.vnc/passwd

COPY ./xstartup.sh /app/.vnc/xstartup
USER root
RUN chown snipe:snipe  /app/.vnc/xstartup && chmod +x /app/.vnc/xstartup
# Autostart firefox (might not be the best way to do it, but it does the trick)
#RUN     bash -c 'echo "node /app/index.js" >> ~/.vnc/xstartup'

USER snipe
