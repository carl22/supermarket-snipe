const { sleep } = require('./shared')
const { PendingXHR } = require('pending-xhr-puppeteer');

const loadHomepage = page => {
  return page.goto('https://www.asda.com/', {waitUntil: 'networkidle2'});
}
const acceptCookies = async page => {
  await page.$eval('.accept-cookies-button', button => button.click())
}

const login = async (page, username, password) => {
  await page.goto('https://www.asda.com/login?redirect_uri=https://groceries.asda.com/&request_origin=gi', {waitUntil: 'networkidle2'});
  await page.focus('input.email-phone-input')
  await page.keyboard.type(username, {delay: 100})
  await page.focus('#password')
  await page.keyboard.type(password, { delay: 100})
  await page.$eval('form button[type=submit]', button => {
    button.click()
  });
}

const loadCollectionPage = async page => {
  await page.waitForSelector('main.home-page', { visible: true, timeout: 0 });
  await page.$eval('a[data-auto-id="btnBookCollection"', button => {
    button.click()
  });
}

const loadDeliveryPage = async page => {
  await page.waitForSelector('main.home-page', { visible: true, timeout: 0 });
  await page.$eval('a[data-auto-id="btnBookDelivery"', button => {
    button.click()
  });
}

const enterLocation = async (page, postcode, houseNumber) => {
  try {
    await page.waitForSelector('.find-address__input-container', { visible: true, timeout: 5000 });
    const postcodeForm = await page.$eval('#textFieldPostcode', field => field !== null)
    await page.focus('#textFieldPostcode')
    await page.keyboard.type(postcode, {delay: 100})
    await page.focus('#textFieldHouse')
    await page.keyboard.type(houseNumber, {delay: 100})
    await page.$eval('button[data-auto-id="btnFind"]', button => {
      button.click()
    });
    await page.waitForSelector('#dropdownSelectAddress', { visible: true, timeout: 2000 });
    const options = await page.$eval('#dropdownSelectAddress', select => {
      return [...select.childNodes].map(o => o.value)
    });
    console.log(options)
    await page.select('#dropdownSelectAddress',options[1])
    await page.$eval('button[data-auto-id="btnSubmit"]', button => {
      button.click()
    });
  } catch (e) {
    console.log(e)
    // input not found
  }
}

const loadNextSet = async page => {
  const pendingXHR = new PendingXHR(page);
  await page.$eval('button[data-auto-id="btnLater"]', button => {
    if(button.classList.contains('asda-btn--disabled') === false) {
      button.click()
    }
  })
  await pendingXHR.waitForAllXhrFinished();
  return await page.$$eval('.co-slots__day', days => days.length)
}

const loadAllSlots = async (page, maxDays) => {
  let dayCount = 0;
  while(dayCount < maxDays) {
    dayCount = await new Promise(async (resolve, reject) => {
      await page.waitForSelector('.co-slots__slots-container', { visible: true, timeout: 10000 });
      const dayCount = await loadNextSet(page)
      await sleep(1)
      resolve(dayCount)
    })
    console.log('looping because day count', dayCount)
  }
}

const checkForSlots = page => {
  return page.$$eval('.co-slots__price-content--box', slots => {
    const validSlots = []
    const text = []
    for(let i = 0; i < slots.length; i++) {
      text.push(slots[i].innerText)
      if(['X','Sold Out','Sold Out\nC&C?'].indexOf(slots[i].innerText) < 0){
        validSlots.push({index: i, text: text})
      }
    }
    return {validSlots, text};
  });
}

const clickSlot = async (page, idx) => {
  console.log('CLICKING SLOT');
  await page.$$eval('.co-slots__price-content--box', (slots, idx) => {
    slots[idx].click();
  }, idx)
  await sleep(5)
  await page.$$eval('.btnContinueCheckout', (continueBtn) => {
    if(continueBtn.length > 0) {
      continueBtn[0].click();
    }
  })

}

const clickTab = (page, tabIndex) => {
  return page.$$eval('.asda-tab',(tabs, tabIndex) => tabs[tabIndex].click(),tabIndex)
}

const searchForOpenSlot = async (page, maxDays, reloadAfterSeconds, focus) => {
  let slots = {validSlots: [], text:[]}
  while(slots.validSlots.length === 0) {
    console.log('loading all slots')
    await loadAllSlots(page, maxDays)
    slots = await checkForSlots(page)
    console.log(focus, slots, slots.text.length)
    const sleepSecs = reloadAfterSeconds * 60
    if(slots.validSlots.length > 0) {
      console.log('found slot')
      return slots;
    }else if(focus === 'deliveryOnly' || focus === 'collectionOnly') {
      console.log('sleeping for ' + sleepSecs + ' secs', new Date());
      await sleep(sleepSecs)
      console.log('reloading page')
      await page.reload();
    }else{
      const notFocus = focus === 'delivery' ?  'collection':'delivery';
      const tabIndex = focus === 'delivery' ?  1 : 0;
      await clickTab(page, tabIndex)
      await sleep(3)
      await loadAllSlots(page, maxDays)
      slots = await checkForSlots(page)
      console.log(notFocus, slots, slots.text.length)
      if(slots.validSlots.length === 0) {
        console.log('sleeping for ' + sleepSecs + ' secs', new Date());
        await sleep(sleepSecs)
        console.log('reloading page')
        await page.reload();
      }else{
        console.log('foudn slot 2')
        return slots
      }
    }
  }
}

const snipe = async (browser, options) => {
  const page = await browser.newPage();
  await page.setViewport({width: 1200, height: 1000});

  await loadHomepage(page)
  await acceptCookies(page)
  await login(page, options.username, options.password)
  if(options.focus === 'delivery' || options.focus === 'deliveryOnly') {
    await loadDeliveryPage(page)
  }else {
    await loadCollectionPage(page, options.postcode, options.houseNumber)
    await enterLocation(page)
  }
  const slots = await searchForOpenSlot(page, options.maxDays, options.checkMins, options.focus)
  await clickSlot(page, slots.validSlots[0].index)
  return slots
}

module.exports = {
  snipe
}
