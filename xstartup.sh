#!/bin/sh

export XKL_XMODMAP_DISABLE=1
autocutsel -fork

# Support clipboard copy/paste functions
vncconfig -nowin &

export QT_XKB_CONFIG_ROOT=/usr/share/X11/xkb

# Uncomment the following two lines for normal desktop:
#unset SESSION_MANAGER
#unset DBUS_SESSION_BUS_ADDRESS

#/usr/bin/mate-session

[ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
[ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
xsetroot -solid grey
vncconfig -iconic &
x-terminal-emulator -geometry 80x24+10+10 -ls -title "$VNCDESKTOP Desktop" &
/app/node_modules/puppeteer/.local-chromium/linux-722234/chrome-linux/chrome &