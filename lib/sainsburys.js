const { PendingXHR } = require('pending-xhr-puppeteer');
const { sleep } = require('./shared')

const login = async (page, username, password) => {
  await page.goto('https://www.sainsburys.co.uk/shop/LogonView/Identifier', {waitUntil: 'networkidle2'});
  await page.focus('input[name="logonId"]')
  await page.keyboard.type(username, {delay: 100})
  await page.$eval('#check-email', button => {
    button.click()
  });
  await page.waitForSelector('#logonPassword', {visible: true, timeout: 0})
  await page.focus('#logonPassword')
  await page.keyboard.type(password, { delay: 100})
  await page.$eval('#login', button => {
    button.click()
  });
  await page.waitForSelector('h1[data-test-id="my-account-title"]', {visible: true, timeout: 10000})
  await page.goto('https://www.sainsburys.co.uk/shop/gb/groceries', {waitUntil: 'networkidle2'});
  await page.waitForSelector('li.navPanelBookDelivery', {visible: true, timeout: 10000})
  await page.$eval('li.navPanelBookDelivery', li => {
    li.childNodes[1].click()
  });
  await page.waitForSelector('#collectBookSlotBtn', {visible: true, timeout: 10000})
  await page.$eval('#collectBookSlotBtn', a => a.click());
}

const chooseWeek = async (page, weekIdx) => {
  const { PendingXHR } = require('pending-xhr-puppeteer');
  await page.$$eval('.deliveryWeekChoice', (containers, weekIdx) => {
    const arrContainers = Array.prototype.slice.call(containers);
    const nodes = Array.prototype.slice.call(arrContainers[0].childNodes);
    const li = nodes.filter(n => n.tagName)
    Array.prototype.slice.call(li[weekIdx].childNodes).filter(n => n.tagName)[0].click()
  } , weekIdx)
  await page.waitForNavigation({ waitUntil: 'networkidle2' })
  await pendingXHR.waitForAllXhrFinished();
}


const parseTable = async page => {
  await page.waitForSelector('table.deliverySlots tbody');
  return await page.$$eval('table.deliverySlots tbody', tbodys => {
    let slots = {validSlots: [], text:[]}
    const tbodysEls = Array.prototype.slice.call(tbodys)
    console.log(tbodysEls)
    tbodysEls.forEach((tbody, tbodyIdx) => {
      const rows = Array.prototype.slice.call(tbody.rows)
      console.log(rows)
      rows.forEach((row, rowIdx) => {
        if(row.classList.contains('timeRangeHeader') === false) {
          const cells = Array.prototype.slice.call(row.cells)
          console.log(cells)
          cells.forEach((cell, cellIdx) => {
            if(cell.classList.contains('timeCol')) {
              slots.text.push(cell.innerHTML)
            }
            if(cell.classList.contains('unavailable') === false) {
              slots.validSlots.push({
                tbodyIdx: tbodyIdx,
                rowIdx: rowIdx,
                cellIdx: cellIdx,
                text: cell.innerHTML
              })
            }
          })
        }
      })
    })
    console.log(slots)
    return slots;
  })
}

const snipe = async (browser, options) => {
  const page = await browser.newPage();
  await page.setViewport({width: 1200, height: 1000});

  await login(page, options.username, options.password)

  await page.waitForSelector('#mapEnabled');
  await page.$$eval('#mapEnabled a.process', as => as[0].click());
  await page.waitForSelector('#slotAreaContainer', {visible: true, timeout: 10000})
  const slots = await parseTable(page)
  console.log(slots)
  // for(let week = 1; week<=3; week++) {
  //   if(week > 1) {
  //     await chooseWeek(page, week - 1)
  //   }
  //   const slots = await parseTable(page)
  //   console.log(slots);
  //   sleep(3)
  // }

  console.log('completed');
}

module.exports = {
  snipe
}
