const { PendingXHR } = require('pending-xhr-puppeteer');
const { sleep } = require('./shared')

const login = async (page, username, password) => {
  await page.goto('https://accounts.groceries.morrisons.com/auth-service/sso/login', {waitUntil: 'networkidle2'});
  await page.focus('#login-input')
  await page.keyboard.type(username, {delay: 100})
  await page.focus('input[type="password"]')
  await page.keyboard.type(password, { delay: 100})
  await page.$eval('#login-submit-button', button => {
    button.click()
  });
}

const clickNext3Days = async page => {
  const pendingXHR = new PendingXHR(page);
  await page.$eval('.next-days', a => a.click())
  await pendingXHR.waitForAllXhrFinished();
  await sleep(1)
  await page.waitForSelector('.next-days', {visible: true, timeout: 10000})
  return await page.$eval('.next-days', a => a.classList.contains('disabled'))
}

const gotoCollectionPage = async page => {
  console.log('seeping')
  await sleep(10)
  console.log('waiting for standard book delivery')
  await page.waitForSelector('.standardBookDelivery', {visible: true, timeout: 0})
  await page.$eval('.standardBookDelivery', button => {
    button.click()
  });
  await sleep(5)
  await page.waitForSelector('.slots-panel', {visible: true, timeout: 0})
}
const snipe = async (browser, options) => {
  const page = await browser.newPage();
  await page.setViewport({width: 1200, height: 1000});

  await login(page, options.username, options.password)
  await gotoCollectionPage(page)

  let hasEnded = false
  do {
    console.log('clicking Next 3 days')
    hasEnded = await clickNext3Days(page)
  }while(hasEnded === false)

  console.log('Sleeping 60')

}

module.exports = {
  snipe
}
