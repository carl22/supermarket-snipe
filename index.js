require('dotenv').config()

const puppeteer = require('puppeteer');

const ASDA = require('./lib/asda');
const TESCO = require('./lib/tesco');
const MORRISONS = require('./lib/morrisons');
const SAINSBURYS = require('./lib/sainsburys');

const SMS = require('./lib/nexmo').init({
  enabled: process.env.NEXMO_ENABLED === 'true',
  key: process.env.NEXMO_KEY,
  secret: process.env.NEXMO_SECRET,
  from: process.env.NEXMO_FROM,
  to: process.env.NEXMO_MOBILE_NUMBER,
});

(async () => {
  const enabledSuperMarkets = process.env.SUPERMARKETS.split(0)
  const browser = await puppeteer.launch({headless: false, devtools: true});

  if(enabledSuperMarkets.indexOf('asda') >= 0) {
    const slots = await ASDA.snipe(browser, {
      username: process.env.ASDA_USER,
      password: process.env.ASDA_PASS,
      postcode: process.env.POSTCODE,
      houseNumber: process.env.HOUSE_NUMBER,
      maxDays: process.env.ASDA_MAX_DAYS,
      checkMins: process.env.CHECK_MINS,
      focus: process.env.ASDA_FOCUS
    })
    SMS.sendMessage('Found ' + slots.validSlots.length + ' Slot(s) On ASDA')
  }

  if(enabledSuperMarkets.indexOf('tesco') >= 0) {
    const slots = await TESCO.snipe(browser, {
      username: process.env.TESCO_USER,
      password: process.env.TESCO_PASS,
      postcode: process.env.POSTCODE,
      houseNumber: process.env.HOUSE_NUMBER,
      checkMins: process.env.CHECK_MINS
    })
    SMS.sendMessage('Found ' + slots.validSlots.length + ' Slot(s) On TESCO')
  }

  if(enabledSuperMarkets.indexOf('morrisons') >= 0) {
    const slots = await MORRISONS.snipe(browser, {
      username: process.env.MORRISONS_USER,
      password: process.env.MORRISONS_PASS,
      postcode: process.env.POSTCODE,
      houseNumber: process.env.HOUSE_NUMBER,
      checkMins: process.env.CHECK_MINS
    })
    SMS.sendMessage('Found ' + slots.validSlots.length + ' Slot(s) On MORRISONS')
  }

  if(enabledSuperMarkets.indexOf('sainsburys') >= 0) {
    const slots = await SAINSBURYS.snipe(browser, {
      username: process.env.SAINSBURYS_USER,
      password: process.env.SAINSBURYS_PASS,
      postcode: process.env.POSTCODE,
      houseNumber: process.env.HOUSE_NUMBER,
      checkMins: process.env.CHECK_MINS
    })
    SMS.sendMessage('Found ' + slots.validSlots.length + ' Slot(s) On SAINSBURYS')
  }

  //await browser.close();
})();
