const { sleep } = require('./shared')
const { PendingXHR } = require('pending-xhr-puppeteer');

const login = async (page, username, password) => {
  const pendingXHR = new PendingXHR(page);
  await page.goto('https://secure.tesco.com/account/en-GB/login?from=https%3A%2F%2Fwww.tesco.com%2Fgroceries%2Fen-GB%2F', {waitUntil: 'networkidle2'});
  await page.focus('#username')
  await page.keyboard.type(username, {delay: 100})
  await page.focus('#password')
  await page.keyboard.type(password, { delay: 100})
  await sleep(3)
  await page.$eval('form button.ui-component__button', button => {
    button.click()
  });
  await pendingXHR.waitForAllXhrFinished();
  await sleep(5)
}

const loadCollectionPage = async page => {
  await page.goto('https://www.tesco.com/groceries/en-GB/slots/collection', {waitUntil: 'networkidle2'});
  await page.waitForSelector('#slot-matrix', { visible: true, timeout: 10000 });
}

const checkSlots = async page => {
  const linkLength = await page.$$eval('.slot-selector--week-tabheader-link', links => links.length)
  const tabbedSlots = []
  for(let tabIdx = 0; tabIdx < linkLength; tabIdx++){
    await sleep(5)
    const pendingXHR = new PendingXHR(page);
    await page.$$eval('.slot-selector--week-tabheader-link', (links, idx) => {
      links[idx].click()
    }, tabIdx)
    await pendingXHR.waitForAllXhrFinished();
    const slots = await page.$$eval('.day-selector__list-item', items => {
      const validSlots = []
      const text = [...items].map((item, i) => {
        if(item.classList.contains('day-selector__list-item--unavailable') === false){
          validSlots.push( {
            tabIdx: tabIdx,
            index: i,
            text: item.getAttribute('data-date')
          })
        }
        return item.getAttribute('data-date')
      })
      return {validSlots, text};
    })
    tabbedSlots.push(slots)
    await sleep(1)
  }
  console.log(tabbedSlots)
  return tabbedSlots;
}

const clickHomeDeliveryTab = async page => {
  const pendingXHR = new PendingXHR(page);
  await page.$eval('.option-tabs--delivery-tabheader-link', link => link.click())
  await pendingXHR.waitForAllXhrFinished();
}

const changeSlotType = async (page, slotType) => {
  const idx = slotType === 'saver' ? 1:0;
  const pendingXHR = new PendingXHR(page);
  await page.$eval('.group-selector--list', (list, idx) => {
    const li = list.childNodes;
    const toSelect = li[idx]
    const link = toSelect.childNodes[0]
    link.click()
  }, idx)
  await pendingXHR.waitForAllXhrFinished();
}

const searchForOpenSlot = async (page, reloadAfterSeconds) => {
  while(1) {
    console.log('checking page');
    const slots = await checkSlots(page)
    const validSlots = slots.filter(s => s.validSlots.length > 0)
    if (validSlots.length > 0) {
      return validSlots[0]
    }

    await sleep(2)
    await clickHomeDeliveryTab(page)
    await sleep(2)
    const homeDeliverySlots = await checkSlots(page)
    const homeDeliveryValidSlots = homeDeliverySlots.filter(s => s.validSlots.length > 0)
    if (homeDeliveryValidSlots.length > 0) {
      return homeDeliveryValidSlots[0]
    }

    await sleep(2)
    await changeSlotType(page, 'saver')
    await sleep(2)
    const homeSaverDeliverySlots = await checkSlots(page)
    const homeSaverDeliveryValidSlots = homeSaverDeliverySlots.filter(s => s.validSlots.length > 0)
    if (homeSaverDeliveryValidSlots.length > 0) {
      return homeSaverDeliveryValidSlots[0]
    }

    const sleepSecs = reloadAfterSeconds * 60
    console.log('sleeping', sleepSecs)
    await sleep(sleepSecs)
    console.log('reloading Page');
    await page.goto('https://www.tesco.com/groceries/en-GB/slots/collection', {waitUntil: 'networkidle2'});
  }
}

const clickSlot = async (page, slot) => {
  await page.$$eval('.slot-selector--week-tabheader-link', (links, slot) => {
    links[slot.tabIdx].click();
  },slot)
  await sleep(2)
  const dayHTML = await page.$$eval('.day-selector__list-item', (items, slot) => {
    const openSlot = items[slot.index];
    return openSlot.innerHTML
  }, slot)
  sleep(2)
  const slotHTML = await page.$eval('.slot-list', sl => sl.innerHTML)
  console.log(dayHTML, slotHTML)
}

const snipe = async (browser, options) => {
  const page = await browser.newPage();
  await page.setViewport({width: 1200, height: 1000});

  await login(page, options.username, options.password)
  await loadCollectionPage(page)
  const validSlots = await searchForOpenSlot(page,options.checkMins)
  if(validSlots.validSlots.length > 0) {
    await clickSlot(page, validSlots)
  }
  return validSlots
}

module.exports = {
  snipe
}
