const Nexmo = require('nexmo');

module.exports = {
  init : options => {
    let nexmo;
    if(options.enabled) {
      nexmo = new Nexmo({
        apiKey: options.key,
        apiSecret: options.secret,
      });
    }
    const sendMessage = message => {
      if(options.enabled) {
        return nexmo.message.sendSms(
          options.from,
          options.to,
          message
        );
      }
    }
    return  {
      sendMessage
    }
  }
}
