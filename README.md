# Supermarket Snipe
This script will login to ASDA and try an grab the first delivery or click and collect at your setup location.

## Quick Setup
Install dependencies
    
    npm i

Copy `.env.example` to `.env` and fill out the the missing parameters. You can choose to enable nexmo to send you a
text message when a slot is found    

## Running the script

    npm run snipe

Once logged in you will need to manually resolve the re-capcha. Just keep doing it and it will eventually give up and
allow you access. Then sit back drink a beer and get alerted when a slot is available.

### Bugs

* Sometimes the reload get stuck. Currently you need to reload manually

### Roadmap

* Get it running in a container so it can be run remotely.
* Add other Supermarkets
* Finish Tesco Snipe - Need to confirm books a slot or what the next steps are
* Finish Morrisons Snipe
* Finish Sainsburys Snipe
* Add focus to tesco, morrisions, sainsburys
